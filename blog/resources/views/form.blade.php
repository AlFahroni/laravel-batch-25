<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>

<body>
    <h1>Create New Account</h1>
    <h2>Form Sign Up</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label> <br>
        <input type="text" name="namaAwal"> <br><br>

        <label>Last Name:</label> <br>
        <input type="text" name="namaAkhir"> <br><br>

        <label>Gender:</label> <br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br><br>

        <label>Nationality:</label><br>
        <select name="National">
            <option value="Indonesian">Indonesia</option>
            <option value="United State">United State</option>
            <option value="Japan">Japan</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Uni Emirates Arab">Uni Emirates Arab</option>
            <option value="Canada">Canada</option>
            <option value="Swiss">Swiss</option>
            <option value="Span">Span</option>
        </select> <br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox">Indonesia
        <input type="checkbox">English
        <input type="checkbox">Arab
        <input type="checkbox">Other<br><br>

        <label>Bio:</label><br>
        <textarea name="bio" id="Bio" placeholder="Isikan semua tentang anda" cols="30" rows="10"></textarea><br><br>

        <input type="submit">
    </form>
</body>

</html>