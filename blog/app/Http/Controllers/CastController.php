<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast')->with('success', 'Data Berhasil Disimpan!');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        // dd($cast);
        return view('cast.index', compact('cast'));
    }

    public function show($cast_id)
    {
        $showcast = DB::table('cast')->where('id', $cast_id)->first();
        // dd($showcast);
        return view('cast.show', compact('showcast'));
    }

    public function edit($cast_id)
    {
        $showcast = DB::table('cast')->where('id', $cast_id)->first();
        // dd($showcast);
        return view('cast.edit', compact('showcast'));
    }

    public function update($cast_id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $cast_id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]);

        return redirect('/cast')->with('success', 'Data Berhasil Di-Update!');
    }

    public function destroy($cast_id)
    {
        $query = DB::table('cast')->where('id', $cast_id)->delete();

        return redirect('/cast')->with('success', 'Data Berhasil Di-Delete!');
    }
}
